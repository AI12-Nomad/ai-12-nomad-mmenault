from typing import List
from src.client.data.data_controller import DataController


class IhmMainCallsDataImpl:
    
    def get_public_games(self) -> List:
        return DataController.available_games