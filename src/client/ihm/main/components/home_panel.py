import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID

from client.ihm.common.component import Component


class HomePanel(Component):
    def __init__(self, pygame_manager: pygame_gui.UIManager) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager

    def render(self) -> None:
        relative_rect = pygame.Rect((0, 0), (1000 * 0.9, 700 * 0.75))
        relative_rect.center = (1000 // 2, 700 // 2)
        self.gui_element = pygame_gui.elements.UIPanel(
            relative_rect=relative_rect,
            manager=self.pygame_manager,
            starting_layer_height=1,
        )

    def modify_text(self, text: str) -> None:
        self.text = text
        self.render()
