from typing import Container
import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID

from client.ihm.common.component import Component


class PlayersList(Component):
    def __init__(
        self,
        pygame_manager: pygame_gui.UIManager,
        container: Component,
        players: list,
        pos_x: int,
        pos_y: int,
    ) -> None:
        super().__init__(pygame_manager, pos_x=pos_x, pos_y=pos_y)
        self.pygame_manager = pygame_manager
        self.container = container
        self.players = players

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIScrollingContainer(
            relative_rect=pygame.Rect((self.pos_x, self.pos_y), (300, 430)),
            manager=self.pygame_manager,
            container=self.container.get_gui_element(),
        )
        self.gui_element.set_scrollable_area_dimensions((280, len(self.players) * 50))

        for i in range(len(self.players)):
            if i % 2 == 0:
                self.add_even(self.players[i], i)
            else:
                self.add_odd(self.players[i], i)

    def modify_text(self, text: str) -> None:
        self.text = text
        self.render()

    def add_even(self, player: str, pos: int) -> None:
        list_panel_even = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((0, 50 * pos), (300, 50)),
            manager=self.pygame_manager,
            starting_layer_height=1,
            container=self.gui_element,
            object_id=ObjectID(class_id="@list_panel_even"),
        )

        image = pygame_gui.elements.UIImage(
            relative_rect=pygame.Rect((10, 5), (40, 40)),
            image_surface=pygame.image.load("../ressources/images/hen.png"),
            manager=self.pygame_manager,
            container=list_panel_even,
        )

        text_rect = pygame.Rect((0, 0), (150, 0))
        text_rect.center = (130, 10)

        text = pygame_gui.elements.UITextBox(
            relative_rect=text_rect,
            html_text=player,
            wrap_to_height=True,
            manager=self.manager,
            container=list_panel_even,
            object_id=ObjectID(class_id="@list_panel_even_text"),
        )

    def add_odd(self, player: str, pos: int) -> None:
        list_panel_odd = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((0, 50 * pos), (300, 50)),
            manager=self.pygame_manager,
            starting_layer_height=1,
            container=self.gui_element,
            object_id=ObjectID(class_id="@list_panel_odd"),
        )

        image = pygame_gui.elements.UIImage(
            relative_rect=pygame.Rect((10, 5), (40, 40)),
            image_surface=pygame.image.load("../ressources/images/hen.png"),
            manager=self.pygame_manager,
            container=list_panel_odd,
        )

        text_rect = pygame.Rect((0, 0), (150, 0))
        text_rect.center = (130, 10)

        text = pygame_gui.elements.UITextBox(
            relative_rect=text_rect,
            html_text=player,
            wrap_to_height=True,
            manager=self.manager,
            container=list_panel_odd,
            object_id=ObjectID(class_id="@list_panel_odd_text"),
        )
