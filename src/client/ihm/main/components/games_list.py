from typing import Container
import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID

from client.ihm.common.component import Component


class GamesList(Component):
    def __init__(
        self,
        pygame_manager: pygame_gui.UIManager,
        container: Component,
        games: list,
        pos_x: int,
        pos_y: int,
    ) -> None:
        super().__init__(pygame_manager, pos_x=pos_x, pos_y=pos_y)
        self.pygame_manager = pygame_manager
        self.container = container
        self.games = games

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIScrollingContainer(
            relative_rect=pygame.Rect((self.pos_x, self.pos_y), (500, 350)),
            manager=self.pygame_manager,
            container=self.container.get_gui_element(),
        )
        self.gui_element.set_scrollable_area_dimensions((480, len(self.games) * 85))

        for i in range(len(self.games)):
            if i % 2 == 0:
                self.add_even(self.games[i], i)
            else:
                self.add_odd(self.games[i], i)

    def modify_text(self, text: str) -> None:
        self.text = text
        self.render()

    def add_even(self, game: tuple, pos: int) -> None:
        list_panel_even = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((0, 85 * pos), (600, 85)),
            manager=self.pygame_manager,
            starting_layer_height=1,
            container=self.gui_element,
            object_id=ObjectID(class_id="@list_panel_even"),
        )

        text = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((10, 0), (150, 0)),
            html_text="Partie "
            + str(pos)
            + "<br>   • "
            + game[0]
            + "<br>   • "
            + game[1],
            wrap_to_height=True,
            manager=self.manager,
            container=list_panel_even,
            object_id=ObjectID(class_id="@list_panel_even_text"),
        )

        if game[0] == "En attente ..." or game[1] == "En attente ...":
            basic_champagne_button = pygame_gui.elements.UIButton(
                relative_rect=pygame.Rect((200, 25), (175, 40)),
                text="Partie en cours",
                container=list_panel_even,
                manager=self.pygame_manager,
                starting_height=1,
                object_id=ObjectID(class_id="@basic_champagne_button"),
            )
            basic_champagne_button.disable()
        else:
            basic_champagne_button = pygame_gui.elements.UIButton(
                relative_rect=pygame.Rect((200, 25), (175, 40)),
                text="Rejoindre",
                container=list_panel_even,
                manager=self.pygame_manager,
                starting_height=1,
                object_id=ObjectID(class_id="@basic_champagne_button"),
            )

    def add_odd(self, game: tuple, pos: int) -> None:
        list_panel_odd = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((0, 85 * pos), (600, 85)),
            manager=self.pygame_manager,
            starting_layer_height=1,
            container=self.gui_element,
            object_id=ObjectID(class_id="@list_panel_odd"),
        )

        text = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((10, 0), (150, 0)),
            html_text="Partie "
            + str(pos)
            + "<br>   • "
            + game[0]
            + "<br>   • "
            + game[1],
            wrap_to_height=True,
            manager=self.manager,
            container=list_panel_odd,
            object_id=ObjectID(class_id="@list_panel_odd_text"),
        )

        if game[0] == "En attente ..." or game[1] == "En attente ...":
            basic_champagne_button = pygame_gui.elements.UIButton(
                relative_rect=pygame.Rect((200, 25), (175, 40)),
                text="Partie en cours",
                container=list_panel_odd,
                manager=self.pygame_manager,
                starting_height=1,
                object_id=ObjectID(class_id="@basic_champagne_button"),
            )
            basic_champagne_button.disable()
        else:
            basic_champagne_button = pygame_gui.elements.UIButton(
                relative_rect=pygame.Rect((200, 25), (175, 40)),
                text="Rejoindre",
                container=list_panel_odd,
                manager=self.pygame_manager,
                starting_height=1,
                object_id=ObjectID(class_id="@basic_champagne_button"),
            )
