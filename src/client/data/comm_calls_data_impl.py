from src.client.data.data_controller import DataController
from src.common.data_structures.profiles import Player
from typing import List

class CommCallsDataImpl:
    def store_connected_user(self, players : List):
        for player in players:
            if player not in DataController.connected_players:
                DataController.connected_players.add(player)
    
    def store_games(self, games : List):
        for game in games:
            if game not in DataController.available_games:
                DataController.available_games.add(game)
